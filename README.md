
# UBlogger

| Branch        | Pipeline          | Code coverage  |  Latest tag  |
| ------------- |:-----------------:|:--------------:| ------------:|
| main       | [![pipeline status](https://gitlab.switch.ch/ub-unibas/go-ublogger/badges/main/pipeline.svg)](https://gitlab.switch.ch/ub-unibas/go-ublogger/-/commits/main)  | [![coverage report](https://gitlab.switch.ch/ub-unibas/go-ublogger/badges/main/coverage.svg)](https://gitlab.switch.ch/ub-unibas/go-ublogger/-/commits/main) | [latest tag](https://gitlab.switch.ch/ub-unibas/go-ublogger/-/tags)


## Description
Log library with multiwriter, can populate standard output, log stash and an output file

## Definition 

```
func CreateUbMultiLogger(host string, port uint64, traceLevel string, filename *tring, namespace string) (logger Logger, logstash *stash.Stash, logFile *os.File, err error)
```

For logStash (if there are no namespace, logStash writer is not created): 
- namespace
- host
- port

Set a default Tracelevel : 
- traceLevel

For a logFile (if there are no filename, logFile is not created) : 
- filename

NB : standard output is always created
NB : if logStash is define, only warning and error logs will be sent to elastic search


## Prerequisite
- To use Logstash you must have logstash running.  
- Please read : https://ub-basel.atlassian.net/wiki/spaces/DLZA/pages/2210562078/Logging for UB-IT Logstash
- You'll have a replace to do in your go.mod : 
```
replace github.com/telkomdev/go-stash => github.com/png-ub/go-stash v0.0.0-20230831094646-7daebd817e31
```
otherwise, you'll see the following logs : 
```
zerolog: could not write event: short write
```

## Usage
### Without TLS
Declare a new logger with its input

```
	var namespace string = "namespace"
	var logStashHost string = "logStashHost"
	var logStashPortNb int = 5000
	var traceLevel string = "traceLevel"
	var logstashtracelevel string = "warnLevel"
	var logFilename string = "logFilename"
	newLogger, logStash, logFile, err := ub_logger.CreateUbMultiLogger(logStashHost, logStashPortNb, traceLevel, logstashtracelevel, logFilename, namespace)
	if err != nil {
		panic(err)
	}
	if logStash != nil {
		defer logStash.Close()
	}
	if logFile != nil {
		defer logFile.Close()
	}
	newLogger.Infof("Creates a multilogger and print")
```

Log a warning
```
newLogger.WarnF("Your warning message: %v", warningMessageVariable)
```

Log a warning with a specific structure named "data"
```
newLogger.Warn().Interface("data", your_data).Msg("your warning)
```

Different tracelevels : 
- TRACE (-1): for tracing the code execution path.
- DEBUG (0): messages useful for troubleshooting the program.
- INFO (1): messages describing the normal operation of an application.
- WARN (2): for logging events that need may need to be checked later.
- ERROR (3): error messages for a specific operation.
- FATAL (4): severe errors where the application cannot recover. os.Exit(1) is called after the message is logged.
- PANIC (5): similar to FATAL, but panic() is called instead.

NB : See other ussage at [zerolog](https://github.com/rs/zerolog) 

### With TLS
Steps 1 and 2 are optional, it depends if you want to use a configuration .toml file which should look like : 
```
trace_level="ERROR"
log_file=""

[stash_config]
#logstash_host="sb-uwf4.swissbib.unibas.ch"
logstash_host="localhost"
logstash_port=5047
logstash_trace_level="ERROR"
namespace="new_logger_test"
use_tls=true
tls_cert="server.cer"
tls_key="server.key"
tls_ca="ca.cert.pem"
dataset="your dataset"
```

1 - Read a config file : 

```
cfgData, err = os.ReadFile(*configParam)
		if err != nil {
			emperror.Panic(errors.Wrapf(err, "cannot read %s", *configParam))
		}
```
2 - load the config file 
```
	// load config file
	cfg, err := ublogger_config.LoadUBLoggerConfig(cfgData)
	if err != nil {
		emperror.Panic(errors.Wrap(err, "cannot decode config file"))
	}
```
3 - use SetTLS with Cert / KEY / CA if any of them is not set, it will default to the ones in this repo
```
		tlsConfig, err := ublogger_config.SetTLS(cfg.StashConfig.TLSCert, cfg.StashConfig.TLSKey, cfg.StashConfig.TLSCA)
		if err != nil {
			fmt.Println("server: loadkeys:", err)
			os.Exit(1)
		}

		newLogger, logStash, logFile, err := ub_logger.CreateUbMultiLoggerTLS(cfg.TraceLevel, logF, ub_logger.SetLogStash(cfg.StashConfig.LogstashHost, cfg.StashConfig.LogstashPort, cfg.StashConfig.Namespace, cfg.StashConfig.LogstashTracelevel), ub_logger.SetTLS(cfg.StashConfig.UseTLS), ub_logger.SetTLSConfig(tlsConfig),ub_logger.SetDataset(cfg.StashConfig.Dataset))
		if err != nil {
			panic(err)
		}
		if logStash != nil {
			defer logStash.Close()
		}
		fmt.Println("logStash", logStash)
		if logFile != nil {
			defer logFile.Close()
		}
```
### Sublogger / add timestamp
To add a timestamp once your logger is created, create a sublogger to call the time stamp with : 
```
	sublogger := r.Logger.With().Str("timestamp", time.Now().String()).Logger()
	sublogger.Warn().Msgf("no matching view (%v)", err)
```
## Support
Please contact Paul Nguyen


## Authors and acknowledgment
Paul Nguyen

## Project status
Development

## Test
Be warned that it can take a bit of time as it tests timeouts  
```
go test -v ./... -coverprofile=coverage.out && go tool cover -html=coverage.out
```

NB : integrated test to real logStash is not done
## Dependencies
### External
- [go-stash](https://github.com/telkomdev/go-stash)
- [zerolog](https://github.com/rs/zerolog) 
### Internal
- [go-testhelpers](https://gitlab.switch.ch/ub-unibas/go-testhelpers)

## GODOC
```
	godoc -http=:6060
```

## Code coverage 
https://ub-unibas.pages.switch.ch/go-ublogger/coverage-report.html

## Licence

## TO DO 