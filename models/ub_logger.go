package models

type UbLoggerConfig struct {
	TraceLevel  string      `toml:"trace_level"`  // trace level for standard output and log file
	LogFile     string      `toml:"log_file"`     // name of the log file generated
	StashConfig StashConfig `toml:"stash_config"` // logstash configuration
}

type StashConfig struct {
	LogstashHost       string `toml:"logstash_host"`        // host of log
	LogstashPort       int    `toml:"logstash_port"`        // server will assume running at this base url
	LogstashTracelevel string `toml:"logstash_trace_level"` // trace level for log stash
	Namespace          string `toml:"namespace"`            // namespace to redirect log to  logstash
	UseTLS             bool   `toml:"use_tls"`              // If use of TLS
	TLSCert            string `toml:"tls_cert"`             // TLS Certificate
	TLSKey             string `toml:"tls_key"`              // TLS Certificate Private Key
	TLSCA              string `toml:"tls_ca"`               // TLS Certificate Authority
	Dataset            string `toml:"dataset"`              // dataset to redirect log to specific dataset logstash
	Hostname           string `toml:"hostname"`             // hostame is to set the hostname
	Caller             bool   `toml:"caller"`               // caller is to set the caller of the logger
}
