package ub_logger

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"

	helper "gitlab.switch.ch/ub-unibas/go-testhelpers"
)

func TestLogger(t *testing.T) {
	testLogger := map[string]struct {
		input  map[string]interface{}
		result string
	}{
		"loggerf info": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "info",
				"logstashtracelevel": "warn",
				"filename":           "test/output/info.log",
				"namespace":          "",
			},
			result: "test/expected/info.log",
		},
		"loggerf trace": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "trace",
				"logstashtracelevel": "warn",
				"filename":           "test/output/trace.log",
				"namespace":          "",
			},
			result: "test/expected/trace.log",
		},
		"loggerf warn": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "warn",
				"logstashtracelevel": "warn",
				"filename":           "test/output/warning.log",
				"namespace":          "",
			},
			result: "test/expected/warning.log",
		},
		"loggerf error": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "warn",
				"logstashtracelevel": "warn",
				"filename":           "test/output/error.log",
				"namespace":          "",
			},
			result: "test/expected/error.log",
		},
		"stdout": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "warn",
				"logstashtracelevel": "warn",
				"filename":           "",
				"namespace":          "",
			},
		},
		"stash connection timed out": {
			input: map[string]interface{}{
				"host":               "sb-uwf4.swissbib.unibas.ch",
				"port":               5046,
				"tracelevel":         "warn",
				"logstashtracelevel": "warn",
				"filename":           "test/output/time_out.log",
				"namespace":          "logger_test",
			},
			// result: []models.FilePath{

			// },
		},
		"stash no such host": {
			input: map[string]interface{}{
				"host":               "test",
				"port":               5046,
				"tracelevel":         "warn",
				"logstashtracelevel": "warn",
				"filename":           "test/output/no_host.log",
				"namespace":          "logger_test",
			},
			// result: []models.FilePath{},
		},
		"panic": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "info",
				"logstashtracelevel": "warn",
				"filename":           "test/output/panic.log",
				"namespace":          "",
			},
			result: "test/expected/panic.log",
		},
		"nofile": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "info",
				"logstashtracelevel": "warn",
				"filename":           "test/fake/nofile.log",
				"namespace":          "",
			},
		},
		"logstash fail": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5001,
				"tracelevel":         "info",
				"logstashtracelevel": "info",
				"filename":           "test/output/no_host.log",
				"namespace":          "logger_test",
			},
		},
		"logstash success": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5002,
				"tracelevel":         "info",
				"logstashtracelevel": "info",
				"filename":           "test/output/no_host.log",
				"namespace":          "logger_test",
			},
		},
		"logstash success integrated": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5046,
				"tracelevel":         "warn",
				"logstashtracelevel": "warn",
				"filename":           "test/output/integrated.log",
				"namespace":          "filepathfrommets",
			},
		},
		"tracelevel_error": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5046,
				"tracelevel":         "warning",
				"logstashtracelevel": "warn",
				"filename":           "test/output/integrated.log",
				"namespace":          "filepathfrommets",
			},
		},
	}
	for name, test := range testLogger {
		test := test
		t.Run(name, func(t *testing.T) {
			if strings.Contains(name, "nofile") {
				helper.TestOsExit(t, "TestLogger", func(t *testing.T) {
					logger, logStash, logFile, err := CreateUbMultiLogger(test.input["host"].(string), test.input["port"].(int), test.input["tracelevel"].(string), test.input["logstashtracelevel"].(string), test.input["filename"].(string), test.input["namespace"].(string))
					if err != nil {
						fmt.Println("err", err)
					}

					if logFile != nil {
						defer logFile.Close()
					}
					if logStash != nil {
						defer logStash.Close()
					}

					logger.Fatalf("Fatal test")
				})
			} else {

				logger, logStash, logFile, err := CreateUbMultiLogger(test.input["host"].(string), test.input["port"].(int), test.input["tracelevel"].(string), test.input["logstashtracelevel"].(string), test.input["filename"].(string), test.input["namespace"].(string))
				if strings.Contains(name, "tracelevel_error") {
					if err.Error() != "Unknown Level String: 'warning', defaulting to NoLevel" {
						t.Errorf("FAILED : TestLogger(%q)  files are differents", test.input)

					} else {
						t.Logf("SUCCEDED")
					}
				}
				if err != nil {
					if logFile != nil {
						defer logFile.Close()
					}
					if logStash != nil {
						defer logStash.Close()
					}
				} else {
					fmt.Println("err", err)
				}

				if strings.Contains(name, "loggerf") {
					logger.Tracef("Trace test")
					logger.Debugf("Debug test")
					logger.Infof("Info test")
					logger.Warnf("Warn test")
					logger.Errorf("Error test")
				}
				if strings.Contains(name, "logstash success") {
					data := map[string]string{
						"title":            "Zip files with same generation DOI",
						"sysnum":           " doiFile.DOI",
						"first_file_name":  "doiFile.Filename",
						"second_file_name": "doiFiles[doi][i-1].Filename",
					}
					logger.Warn().Interface("data", data).Msg("logstash success Warning same generation doiFiles[doi]")
					logger.Infof("logstash success")
				}
				if strings.Contains(name, "panic") {
					defer func() {
						if r := recover(); r == nil {
							fmt.Println("Recovered in f", r)
						}
					}()
					logger.Panicf("panic test")
					t.Errorf("The code did not panic")
				}
				if strings.Contains(name, "fatal") {
					helper.TestOsExit(t, "TestLogger", func(t *testing.T) {
						logger.Fatalf("Fatal test")
					})
				}
				if test.result != "" {
					got := CompareLines(test.input["filename"].(string), test.result)
					if !got {
						t.Errorf("FAILED : TestLogger(%q)  files are differents", test.input)
					} else {
						t.Logf("SUCCEDED")
					}
				}
				if test.input["filename"].(string) != "" {
					err := os.Remove(test.input["filename"].(string))
					if err != nil {
						logger.Fatalf(err.Error())
					}
				}
			}
		})
	}
}

func TestLoggerTLS(t *testing.T) {
	testLogger := map[string]struct {
		input  map[string]interface{}
		result string
	}{
		"loggerf info": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "info",
				"logstashtracelevel": "warn",
				"filename":           "test/output/info.log",
				"namespace":          "",
			},
			result: "test/expected/info.log",
		},
		"nofile": {
			input: map[string]interface{}{
				"host":               "",
				"port":               0,
				"tracelevel":         "info",
				"logstashtracelevel": "warn",
				"filename":           "test/fake2/nofile2.log",
				"namespace":          "",
			},
		},
		// "stash no such host": {
		// 	input: map[string]interface{}{
		// 		"host":               "test",
		// 		"port":               5046,
		// 		"tracelevel":         "warn",
		// 		"logstashtracelevel": "warn",
		// 		"filename":           "test/output/no_host.log",
		// 		"namespace":          "logger_test",
		// 	},
		// },
		"TLS fails": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5435,
				"tracelevel":         "info",
				"logstashtracelevel": "info",
				"filename":           "test/output/no_host.log",
				"namespace":          "logger_test",
			},
		},
		"TLS success": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5046,
				"tracelevel":         "warn",
				"logstashtracelevel": "warn",
				"filename":           "test/output/integrated.log",
				"namespace":          "filepathfrommets",
			},
		},
		"TLS traceLevel error": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5046,
				"tracelevel":         "warning",
				"logstashtracelevel": "warn",
				"filename":           "test/output/integrated.log",
				"namespace":          "filepathfrommets",
			},
		},
		"TLS logstashtracelevel error": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5046,
				"tracelevel":         "warn",
				"logstashtracelevel": "warning",
				"filename":           "test/output/integrated.log",
				"namespace":          "filepathfrommets",
			},
		},
	}

	for name, test := range testLogger {
		test := test
		t.Run(name, func(t *testing.T) {
			if strings.Contains(name, "nofile") {
				helper.TestOsExit(t, "TestLoggerTLS", func(t *testing.T) {
					logger, logStash, logFile, err := CreateUbMultiLoggerTLS(test.input["tracelevel"].(string), test.input["filename"].(string), SetLogStash(test.input["host"].(string), test.input["port"].(int), test.input["namespace"].(string), test.input["logstashtracelevel"].(string)))
					if err != nil {
						t.Errorf("FAILED : TestLoggerTLS(%q)  there is an error %s", test.input, err.Error())
					}
					if logFile != nil {
						defer logFile.Close()
					}
					if logStash != nil {
						defer logStash.Close()
					}
					logger.Fatalf("Fatal test")
				})
			} else if strings.Contains(name, "TLS fails") {
				tlsConfig := &tls.Config{Certificates: []tls.Certificate{}}
				logger, logStash, logFile, err := CreateUbMultiLoggerTLS(test.input["tracelevel"].(string), test.input["filename"].(string), SetLogStash(test.input["host"].(string), test.input["port"].(int), test.input["namespace"].(string), test.input["logstashtracelevel"].(string)), SetTLS(true), SetTLSConfig(tlsConfig), SetKeepAlive(time.Second*3),
					SetReadTimeout(time.Second*3),
					SetWriteTimeout(time.Second*3), SetSkipVerify(false))
				if err != nil {
					t.Errorf("FAILED : TestLoggerTLS(%q)  there is an error %s", test.input, err.Error())
				}
				if logFile != nil {
					defer logFile.Close()
				}
				if logStash != nil {
					defer logStash.Close()
				}
				logger.Warnf("TLS fails")
			} else if strings.Contains(name, "TLS success") {
				cer, err := tls.LoadX509KeyPair("test/certs/server.cer", "test/certs/server.key")
				if err != nil {
					t.Fatalf("server: loadkeys: %s", err)
					os.Exit(1)
				}

				tlsConfig := &tls.Config{Certificates: []tls.Certificate{cer}}
				logger, logStash, logFile, err := CreateUbMultiLoggerTLS(test.input["tracelevel"].(string), test.input["filename"].(string), SetLogStash(test.input["host"].(string), test.input["port"].(int), test.input["namespace"].(string), test.input["logstashtracelevel"].(string)), SetTLS(true), SetTLSConfig(tlsConfig), SetKeepAlive(time.Second*3),
					SetReadTimeout(time.Second*3),
					SetWriteTimeout(time.Second*3), SetSkipVerify(true))
				if err != nil {
					t.Errorf("FAILED : TestLoggerTLS(%q)  there is an error %s", test.input, err.Error())
				}
				if logFile != nil {
					defer logFile.Close()
				}
				if logStash != nil {
					defer logStash.Close()
				}
				data := map[string]string{
					"title":            "Zip files with same generation DOI",
					"sysnum":           " doiFile.DOI",
					"first_file_name":  "doiFile.Filename",
					"second_file_name": "doiFiles[doi][i-1].Filename",
				}
				logger.Warn().Interface("data", data).Msg("logstash success Warning same generation doiFiles[doi]")
				logger.Warnf("TLS Success test")
			} else {
				logger, logStash, logFile, err := CreateUbMultiLoggerTLS(test.input["tracelevel"].(string), test.input["filename"].(string), SetLogStash(test.input["host"].(string), test.input["port"].(int), test.input["namespace"].(string), test.input["logstashtracelevel"].(string)), SetKeepAlive(time.Second*3),
					SetReadTimeout(time.Second*3),
					SetWriteTimeout(time.Second*3), SetTLS(false))
				if strings.Contains(name, "tracelevel_error") {
					if err.Error() != "Unknown Level String: 'warning', defaulting to NoLevel" {
						t.Errorf("FAILED : TestLoggerTLS(%q)  files are differents", test.input)

					} else {
						t.Logf("SUCCEDED")
					}
				}
				if err != nil {
					fmt.Println("err", err)
				}
				if logFile != nil {
					defer logFile.Close()
				}
				if logStash != nil {
					defer logStash.Close()
				}
				if strings.Contains(name, "loggerf") {
					logger.Tracef("Trace test")
					logger.Debugf("Debug test")
					logger.Infof("Info test")
					logger.Warnf("Warn test")
					logger.Errorf("Error test")
				}

				if strings.Contains(name, "panic") {
					defer func() {
						if r := recover(); r == nil {
							fmt.Println("Recovered in f", r)
						}
					}()
					logger.Panicf("panic test")
					t.Errorf("The code did not panic")
				}
				if strings.Contains(name, "fatal") {
					helper.TestOsExit(t, "TestLogger", func(t *testing.T) {
						logger.Fatalf("Fatal test")
					})
				}
				if test.result != "" {
					got := CompareLines(test.input["filename"].(string), test.result)
					if !got {
						t.Errorf("FAILED : TestLoggerTLS(%q)  files are differents", test.input)
					} else {
						t.Logf("SUCCEDED")
					}
				}
				if test.input["filename"].(string) != "" {
					err := os.Remove(test.input["filename"].(string))
					if err != nil {
						logger.Fatalf(err.Error())
					}
				}
			}
		})
	}
}

func TestLoggerTLSPresentation(t *testing.T) {
	testLogger := map[string]struct {
		input  map[string]interface{}
		result string
	}{
		"loggerf info": {
			input: map[string]interface{}{
				"host":               "localhost",
				"port":               5047,
				"tracelevel":         "info",
				"logstashtracelevel": "warn",
				"filename":           "test/output/test.log",
				"namespace":          "logger_test",
				"dataset":            "test",
			},
			result: "test/expected/info.log",
		},
	}

	for name, test := range testLogger {
		test := test
		t.Run(name, func(t *testing.T) {
			// logger, logStash, logFile := CreateUbMultiLoggerTLS(test.input["tracelevel"].(string), test.input["filename"].(string), SetLogStash(test.input["host"].(string), test.input["port"].(int), test.input["namespace"].(string), test.input["logstashtracelevel"].(string)), SetTLS(false))
			// if logFile != nil {
			// 	defer logFile.Close()
			// }
			// if logStash != nil {
			// 	defer logStash.Close()
			// }
			// data := map[string]string{
			// 	"title":            "This is a test",
			// 	"sysnum":           " doiFile.DOI",
			// 	"first_file_name":  "doiFile.Filename",
			// 	"second_file_name": "doiFiles[doi][i-1].Filename",
			// }
			// logger.Warn().Interface("data", data).Msg("This is a test")
			// logger.Warnf("TLS Success test")

			logger, logStash, logFile, err := CreateUbMultiLoggerTLS(test.input["tracelevel"].(string), test.input["filename"].(string), SetLogStash(test.input["host"].(string), test.input["port"].(int), test.input["namespace"].(string), test.input["logstashtracelevel"].(string)), SetTLS(true))
			if err != nil {

				t.Errorf("FAILED : TestLoggerTLSPresentation(%q)  error : %s", test.input, err.Error())

			}
			if logFile != nil {
				defer logFile.Close()
			}
			if logStash != nil {
				defer logStash.Close()
			}
			data := map[string]string{
				"title":            "Zip files with same generation DOI",
				"sysnum":           " doiFile.DOI",
				"first_file_name":  "doiFile.Filename",
				"second_file_name": "doiFiles[doi][i-1].Filename",
			}
			logger.Warn().Interface("data", data).Msg("logstash success Warning same generation doiFiles[doi]")
			logger.Warnf("TLS Success test")
		})
	}
}

func CompareLines(file1, file2 string) bool {
	p, err := ioutil.ReadFile(file1)
	if err != nil {
		fmt.Println("ReadFile err : ", err)
	}
	p2, err := ioutil.ReadFile(file2)
	if err != nil {
		fmt.Println("ReadFile err : ", err)
	}
	for i, line := range bytes.Split(p, []byte{'\n'}) {
		var v, v2 struct {
			level   string
			message string
		}
		if len(line) > 0 {
			if err := json.Unmarshal(line, &v); err != nil {
				fmt.Println("Unmarshal v err : ", err)
			}
			if err := json.Unmarshal(bytes.Split(p2, []byte{'\n'})[i], &v2); err != nil {
				fmt.Println("Unmarshal v2 verr : ", err)
			}
			if !reflect.DeepEqual(v, v2) {
				return false
			}
		}
	}
	return true
}
