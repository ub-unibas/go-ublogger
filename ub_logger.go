// Package ub_logger provides a creation for a multilogger for standard output, file and logstash, with of without TLS connection
package ub_logger

import (
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/telkomdev/go-stash"
)

type Logger struct {
	*zerolog.Logger
}

type NewStash struct{ *stash.Stash }

func (s *NewStash) Write(data []byte) (int, error) {
	l := len(data)
	_, err := s.Stash.Write(data)
	return l, err
}

// CreateUbMultiLogger returns at least a standard output logger.
// If a host, port and namespace are filled a logstash logger is created.
// If a filename is filled a logFile ist created
func CreateUbMultiLogger(host string, port int, traceLevel, logstashtraceLevel, filename, namespace string) (logger *Logger, logstash *NewStash, logFile *os.File, logerr error) {
	var multiWriter io.Writer
	var stashError, fileError, err error
	var stashOpt []stash.Option
	o := &options{
		dialer: &net.Dialer{
			KeepAlive: time.Second * 3,
		},
		writeTimeout: 30 * time.Second,
		readTimeout:  30 * time.Second,
	}
	if o.readTimeout != 0 {
		stashOpt = append(stashOpt, stash.SetReadTimeout(o.readTimeout))
	}
	if o.writeTimeout != 0 {
		stashOpt = append(stashOpt, stash.SetWriteTimeout(o.writeTimeout))
	}
	if o.dialer != nil && o.dialer.KeepAlive != 0 {
		stashOpt = append(stashOpt, stash.SetKeepAlive(o.keepAlive))
	}
	logstashlevel := zerolog.WarnLevel

	ws := make([]io.Writer, 0)
	ws = append(ws, os.Stdout)

	if filename != "" {
		logFile, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fileError = err
		} else {
			ws = append(ws, logFile)
		}
	}

	if traceLevel != "" {
		level, err := zerolog.ParseLevel(traceLevel)
		if err == nil {
			zerolog.SetGlobalLevel(level)
		} else {
			logerr = err
			return
		}

	}
	if logstashtraceLevel != "" {
		logstashlevel, err = zerolog.ParseLevel(logstashtraceLevel)
		if err != nil {
			logstashlevel = zerolog.WarnLevel
		}
	}
	if host != "" && port != 0 && namespace != "" {
		clogstash, err := stash.Connect(host, uint64(port), stashOpt...)
		if err != nil {
			stashError = err
		} else {
			logstash = &NewStash{Stash: clogstash}
		}

		//redirect all logs above Warning level to logstash if logstash exists
		if logstash != nil {

			wsStash := &zerolog.FilteredLevelWriter{
				Writer: zerolog.LevelWriterAdapter{logstash},
				Level:  logstashlevel,
			}
			ws = append(ws, wsStash)
		}
	}

	// Create a multi-writer that adds all writers
	multiWriter = zerolog.MultiLevelWriter(ws...)

	zerologger := zerolog.New(multiWriter).With().Timestamp().Logger()
	if logstash != nil {
		zerologger = zerolog.New(multiWriter).With().Dict("data_stream", zerolog.Dict().Str("namespace", namespace)).Logger()
	}

	logger = &Logger{&zerologger}
	if stashError != nil {
		logger.Errorf("stash.Connect error : %v" + stashError.Error())
	}
	if fileError != nil {
		logger.Errorf("Openfile error : %v" + fileError.Error())
	}
	return
}

type options struct {
	host               string
	port               int
	namespace          string
	logstashTraceLevel string
	dialer             *net.Dialer
	useTLS             bool
	skipVerify         bool
	readTimeout        time.Duration
	writeTimeout       time.Duration
	tlsConfig          *tls.Config
	keepAlive          time.Duration
	dataset            string
	caller             bool
	hostname           string
}

// Option function
type Option func(*options)

// CreateUbMultiLoggerTLS returns at least a standard output logger.
// If a host, port and namespace are filled a logstash logger is created, it supports TLS connection .
// If a filename is filled a logFile ist created
func CreateUbMultiLoggerTLS(traceLevel, filename string, opts ...Option) (logger *Logger, logstash *NewStash, logFile *os.File, logerr error) {
	var multiWriter io.Writer
	var stashError, fileError, err error
	var stashOpt []stash.Option
	o := &options{
		dialer: &net.Dialer{
			KeepAlive: time.Second * 30,
		},
		writeTimeout: 30 * time.Second,
		readTimeout:  30 * time.Second,
	}

	logstashlevel := zerolog.WarnLevel

	ws := make([]io.Writer, 0)
	csw := zerolog.NewConsoleWriter(func(w *zerolog.ConsoleWriter) {
		w.TimeFormat = time.RFC3339
		w.Out = os.Stdout
	})
	ws = append(ws, csw)
	for _, option := range opts {
		option(o)
	}

	if o.readTimeout != 0 {
		stashOpt = append(stashOpt, stash.SetReadTimeout(o.readTimeout))
	}
	if o.writeTimeout != 0 {
		stashOpt = append(stashOpt, stash.SetWriteTimeout(o.writeTimeout))
	}
	if o.dialer != nil && o.dialer.KeepAlive != 0 {
		stashOpt = append(stashOpt, stash.SetKeepAlive(o.keepAlive))
	}
	if filename != "" {
		logFile, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fileError = err
		} else {
			ws = append(ws, logFile)
		}
	}

	if traceLevel != "" {
		level, err := zerolog.ParseLevel(traceLevel)
		if err == nil {
			zerolog.SetGlobalLevel(level)
		} else {
			logerr = err
			return
		}

	}
	if o.logstashTraceLevel != "" {
		logstashlevel, err = zerolog.ParseLevel(o.logstashTraceLevel)
		if err != nil {
			logstashlevel = zerolog.WarnLevel
			logerr = err
			return
		}
	}

	if o.host != "" && o.port != 0 && o.namespace != "" {
		clogstash, err := stash.Connect(o.host, uint64(o.port), stashOpt...)
		if err != nil {
			stashError = err
		} else {
			logstash = &NewStash{Stash: clogstash}
		}

		if o.useTLS {

			if o.tlsConfig != nil {
				stashOpt = append(stashOpt, stash.SetTLSConfig(o.tlsConfig))
			}
			if o.skipVerify {
				stashOpt = append(stashOpt, stash.SetSkipVerify(o.skipVerify))
			} else {
				stashOpt = append(stashOpt, stash.SetSkipVerify(false))
			}

			stashOpt = append(stashOpt, stash.SetTLS(true))

			clogstash, err = stash.Connect(o.host, uint64(o.port), stashOpt...)
			// logstash, err = stash.Connect(o.host, uint64(o.port), stash.SetTLSConfig(o.tlsConfig), stash.SetTLS(true))
			if err != nil {
				stashError = err
			} else {
				logstash = &NewStash{Stash: clogstash}
			}

		}

		//redirect all logs above Warning level to logstash if logstash exists
		if logstash != nil {

			wsStash := &zerolog.FilteredLevelWriter{
				Writer: zerolog.LevelWriterAdapter{logstash},
				Level:  logstashlevel,
			}
			ws = append(ws, wsStash)
		}
	}

	// Create a multi-writer that adds all writers
	multiWriter = zerolog.MultiLevelWriter(ws...)

	zerologger := zerolog.New(multiWriter).With().Timestamp().Logger()
	if o.caller {
		zerologger = zerolog.New(multiWriter).With().Timestamp().Caller().Logger()
	}
	if o.hostname != "" {
		zerologger = zerologger.With().Timestamp().Str("host", o.hostname).Logger()

	}
	if logstash != nil {
		if o.dataset != "" {
			zerologger = zerologger.With().Dict("data_stream", zerolog.Dict().Str("namespace", o.namespace).Str("dataset", o.dataset)).Logger()
		} else {
			zerologger = zerologger.With().Dict("data_stream", zerolog.Dict().Str("namespace", o.namespace)).Logger()
		}
	}

	logger = &Logger{&zerologger}
	if stashError != nil {
		logger.Errorf("stash.Connect error : %v" + stashError.Error())
	}
	if fileError != nil {
		logger.Errorf("Openfile error : %v" + fileError.Error())
	}
	return
}

// SetLogStash Option func
func SetLogStash(host string, port int, namespace, logstashlevel string) Option {
	return func(o *options) {
		if host != "" {
			o.host = host
		}
		if port != 0 {
			o.port = port
		}
		if namespace != "" {
			o.namespace = namespace
		}
		if logstashlevel != "" {
			o.logstashTraceLevel = logstashlevel
		}
	}
}

// SetTLS Option func
func SetTLS(useTLS bool) Option {
	return func(o *options) {
		o.useTLS = useTLS
	}
}

// SetTLSConfig Option func
func SetTLSConfig(config *tls.Config) Option {
	return func(o *options) {
		if config != nil {
			o.tlsConfig = config
		}

	}
}

// SetSkipVerify Option func
func SetSkipVerify(skipVerify bool) Option {
	return func(o *options) {
		o.skipVerify = skipVerify
	}
}

// SetReadTimeout Option func
func SetReadTimeout(readTimeout time.Duration) Option {
	return func(o *options) {
		o.readTimeout = readTimeout
	}
}

// SetWriteTimeout Option func
func SetWriteTimeout(writeTimeout time.Duration) Option {
	return func(o *options) {
		o.writeTimeout = writeTimeout
	}
}

// SetKeepAlive Option func
func SetKeepAlive(keepAlive time.Duration) Option {
	return func(o *options) {
		o.dialer.KeepAlive = keepAlive
	}
}

// SetDataset Option func
func SetDataset(dataset string) Option {
	return func(o *options) {
		if dataset != "" {
			o.dataset = dataset
		}
	}
}

// SetTimestamp Option func
func SetCaller(caller bool) Option {
	return func(o *options) {
		if caller {
			o.caller = caller
		}
	}
}

// SetHostname Option func
func SetHostname(hostname string) Option {
	return func(o *options) {
		if hostname != "" {
			o.hostname = hostname
		}
	}
}

func (log Logger) Tracef(format string, a ...interface{}) {
	log.Trace().Msg(fmt.Sprintf(format, a...))
}
func (log Logger) Debugf(format string, a ...interface{}) {
	log.Debug().Msg(fmt.Sprintf(format, a...))
}
func (log Logger) Infof(format string, a ...interface{}) {
	log.Info().Msg(fmt.Sprintf(format, a...))
}
func (log Logger) Warnf(format string, a ...interface{}) {
	log.Warn().Msg(fmt.Sprintf(format, a...))
}
func (log Logger) Errorf(format string, a ...interface{}) {
	log.Error().Msg(fmt.Sprintf(format, a...))
}
func (log Logger) Fatalf(format string, a ...interface{}) {
	log.Fatal().Msg(fmt.Sprintf(format, a...))
}
func (log Logger) Panicf(format string, a ...interface{}) {
	log.Panic().Msg(fmt.Sprintf(format, a...))
}
