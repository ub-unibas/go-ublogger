module gitlab.switch.ch/ub-unibas/go-ublogger/v2

go 1.22

require (
	emperror.dev/errors v0.8.1
	github.com/BurntSushi/toml v1.3.2
	github.com/rs/zerolog v1.30.1-0.20230905091805-1bac5cca5027
	github.com/telkomdev/go-stash v1.0.3
	gitlab.switch.ch/ub-unibas/go-testhelpers v0.0.0-20231004070116-a92f04ad03a5
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)

// replace github.com/telkomdev/go-stash => github.com/png-ub/go-stash v0.0.0-20230831094646-7daebd817e31
