package ub_logger_test

import (
	"crypto/tls"

	ub_logger "gitlab.switch.ch/ub-unibas/go-ublogger/v2"
)

func ExampleCreateUbMultiLogger() {
	var namespace string = "namespace"
	var logStashHost string = "logStashHost"
	var logStashPortNb int = 5000
	var traceLevel string = "traceLevel"
	var logstashtracelevel string = "warnLevel"
	var logFilename string = "logFilename"
	newLogger, logStash, logFile, err := ub_logger.CreateUbMultiLogger(logStashHost, logStashPortNb, traceLevel, logstashtracelevel, logFilename, namespace)
	if err != nil {
		panic(err)
	}
	if logStash != nil {
		defer logStash.Close()
	}
	if logFile != nil {
		defer logFile.Close()
	}

	newLogger.Infof("Creates a multilogger and print")
	// Ouput:
	// newlogger: {"level":"trace","timestamp":"2023-09-06 13:56:51.490774605 +0200 CEST m=+129.380533925","message":"Creates a multilogger and print"}
}

func ExampleCreateUbMultiLoggerTLS() {
	var namespace string = "namespace"
	var logStashHost string = "logStashHost"
	var logStashPortNb int = 5000
	var traceLevel string = "traceLevel"
	var logstashtracelevel string = "warnLevel"
	var logFilename string = "logFilename"
	newLogger, logStash, logFile, err := ub_logger.CreateUbMultiLoggerTLS(traceLevel, logFilename, ub_logger.SetLogStash(logStashHost, logStashPortNb, namespace, logstashtracelevel), ub_logger.SetTLS(true), ub_logger.SetTLSConfig(&tls.Config{}))

	if err != nil {
		panic(err)
	}

	if logStash != nil {
		defer logStash.Close()
	}
	if logFile != nil {
		defer logFile.Close()
	}

	newLogger.Infof("Creates a multilogger with TLS and print")
	// Ouput:
	// newlogger: {"level":"trace","timestamp":"2023-09-06 13:56:51.490774605 +0200 CEST m=+129.380533925","message":"Creates a multilogger with TLS and print"}
}
