package config

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/fs"
	"os"

	"emperror.dev/errors"
	"github.com/BurntSushi/toml"
	"gitlab.switch.ch/ub-unibas/go-ublogger/v2/certs"
	"gitlab.switch.ch/ub-unibas/go-ublogger/v2/models"
)

func LoadUBLoggerConfig(data []byte) (*models.UbLoggerConfig, error) {

	cfg := &models.UbLoggerConfig{
		TraceLevel: "WARNING",
		LogFile:    "",
		StashConfig: models.StashConfig{
			LogstashHost:       "",
			LogstashPort:       0,
			LogstashTracelevel: "WARNING",
			Namespace:          "",
			UseTLS:             false,
			TLSKey:             "",
			TLSCert:            "",
			TLSCA:              "",
			Dataset:            "",
			Hostname:           "",
			Caller:             false,
		},
	}
	if _, err := toml.Decode(string(data), cfg); err != nil {
		return nil, errors.Wrap(err, "cannot decode toml config")
	}
	return cfg, nil
}

func SetTLS(tlsCert, key, certAuthority string) (*tls.Config, error) {
	var cer tls.Certificate
	var err error
	var addCA = []*x509.Certificate{}
	if tlsCert == "" || key == "" || certAuthority == "" {
		fmt.Println("with default files from FS")
		certBytes, err := fs.ReadFile(certs.CertFS, "localhost.cert.pem")
		if err != nil {
			return nil, err
		}
		keyBytes, err := fs.ReadFile(certs.CertFS, "localhost.key.pem")
		if err != nil {
			return nil, err
		}
		if cer, err = tls.X509KeyPair(certBytes, keyBytes); err != nil {
			return nil, err
		}

		rootCABytes, err := fs.ReadFile(certs.CertFS, "ca.cert.pem")
		if err != nil {
			return nil, err
		}
		block, _ := pem.Decode(rootCABytes)
		if block == nil {
			return nil, errors.Errorf("pem.Decode nil")
		}
		rootCA, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return nil, err
		}
		addCA = append(addCA, rootCA)
	} else {
		cer, err = tls.LoadX509KeyPair(tlsCert, key)
		if err != nil {
			return nil, err
		}
		rootCABytes, err := os.ReadFile(certAuthority)
		if err != nil {
			return nil, err
		}
		block, _ := pem.Decode(rootCABytes)
		if block == nil {
			return nil, errors.Errorf("pem.Decode nil")
		}
		rootCA, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return nil, err
		}
		addCA = append(addCA, rootCA)

	}
	rootCAs, err := x509.SystemCertPool()
	if err != nil {
		return nil, err
	}
	if rootCAs == nil {
		rootCAs = x509.NewCertPool()
	}
	for _, ca := range addCA {
		rootCAs.AddCert(ca)
	}
	return &tls.Config{Certificates: []tls.Certificate{cer},
		RootCAs: rootCAs}, nil
}
